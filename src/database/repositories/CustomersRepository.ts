import AppError from "../../utils/AppError";
import { Op } from "sequelize";
import { Query } from "../../shared/types/query";
import { getPagination } from "../../utils/GetPagination";
import model, { CustomerInput, CustomerOutput } from "../models/CustomersModel";

// getAll retorna rows e count através do metódo findAndCountAll
export const getAll = async (
    customerName: string, 
    lowerCreditLimit: number, 
    higherCreditLimit: number, 
    query: Query
): Promise<{rows: CustomerOutput[], count: number}> => {
    let {size, page, sort, order, ...filters} = query;

    const id = "customerNumber"; // definindo o id da entidade
    const {...pagination} = getPagination(id, query); // spread operator para alocar o retorno de getPagination

    // Valores default caso parâmetros não sejam passados
    if (!customerName) customerName = "";
    if (!lowerCreditLimit) lowerCreditLimit = 0;
    if (!higherCreditLimit) higherCreditLimit = 1000000;

    return await model.findAndCountAll({
        where: {
            customerName: {[Op.like]: `%${customerName}%`}, 
            creditLimit: {[Op.between]: [lowerCreditLimit, higherCreditLimit]},
            ...filters
        },
        ...pagination,
    });
};

export const getById = async (id: number): Promise<CustomerOutput> => {
    const customer = await model.findOne({
        where: {
            customerNumber: id,
        },
    });

    if (!customer) {
        throw new AppError("NotFoundError", "Registro não encotrado", 404);
    };

    return customer;
};

export const create = async (payload: CustomerInput): Promise<CustomerOutput> => {
    return await model.create(payload);
};

export const updateById = async (id: number, payload: CustomerInput): Promise<CustomerOutput> => {
    const customer = await model.findByPk(id);

    if (!customer) {
        throw new Error("Registro não encontrado");
    };

    return await customer.update(payload);
};

export const deleteById = async (id: number): Promise<void> => {
    const customer = await model.findByPk(id);

    if (!customer) {
        throw new Error("Registro não encontrado");
    };

    await customer.destroy();
};