import AppError from "../../utils/AppError";
import { Op } from "sequelize";
import { Query } from "../../shared/types/query";
import { getPagination } from "../../utils/GetPagination";
import model, {ProductInput, ProductOutput} from "../models/ProductsModel";

export const getAll = async (
    lowerQuantityInStock: number,
    higherQuantityInStock: number,
    query: Query
): Promise<{rows: ProductOutput[], count: number}> => {
    let {size, page, sort, order, ...filters} = query;

    const id = "productCode"; // definindo o id da entidade
    const {...pagination} = getPagination(id, query); // spread operator para alocar o retorno de getPagination

    if (!lowerQuantityInStock) lowerQuantityInStock = 0;
    if (!higherQuantityInStock) higherQuantityInStock = 10000;

    return await model.findAndCountAll({
        where: {
            quantityInStock: {[Op.between]: [lowerQuantityInStock, higherQuantityInStock]},
            ...filters
        },
        ...pagination,
    });
};

export const getById = async (id: string): Promise<ProductOutput> => {
    const product = await model.findOne({
        where: {
            productCode: id,
        },
    });

    if (!product) {
        throw new AppError("NotFoundError", "Registro não encotrado", 404);
    };

    return product;
};

export const create = async (payload: ProductInput): Promise<ProductOutput> => {
    return await model.create(payload);
};

export const updateById = async (id: string, payload: ProductInput): Promise<ProductOutput> => {
    const product = await model.findByPk(id);

    if (!product) {
        throw new Error("Registro não encontrado");
    };

    return await product.update(payload);
};

export const deleteById = async (id: string): Promise<void> => {
    const product = await model.findByPk(id);

    if (!product) {
        throw new Error("Registro não encontrado");
    };

    await product.destroy();
};