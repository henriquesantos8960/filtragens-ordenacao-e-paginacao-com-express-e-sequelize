import AppError from "../../utils/AppError";
import { Op } from "sequelize";
import { Query } from "../../shared/types/query";
import { getPagination } from "../../utils/GetPagination";
import model, { OrderInput, OrderOutput } from "../models/OrdersModel";


export const getAll = async (
    lowerOrderDate: string, 
    higherOrderDate: string, 
    query: Query
): Promise<{rows: OrderOutput[], count: number}> => {
    let {size, page, sort, order, ...filters} = query;

    const id = "orderNumber"; // definindo o id da entidade
    const {...pagination} = getPagination(id, query); // spread operator para alocar o retorno de getPagination

    if (!lowerOrderDate) lowerOrderDate = "2003-01-01";
    if (!higherOrderDate) higherOrderDate = "2999-01-01";

    return await model.findAndCountAll({
        where: {
            orderDate: {[Op.between]: [lowerOrderDate, higherOrderDate]},
            ...filters
        },
        ...pagination,
    });
};

export const getById = async (id: number): Promise<OrderOutput> => {
    const order = await model.findOne({
        where: {
            orderNumber: id,
        },
        include: {all: true},
    });

    if (!order) {
        throw new AppError("NotFoundError", "Registro não encotrado", 404);
    };

    return order;
};

export const create = async (payload: OrderInput): Promise<OrderOutput> => {
    return await model.create(payload);
};

export const updateById = async (id: number, payload: OrderInput): Promise<OrderOutput> => {
    const order = await model.findByPk(id);

    if (!order) {
        throw new Error("Registro não encontrado");
    };

    return await order.update(payload);
};

export const deleteById = async (id: number): Promise<void> => {
    const order = await model.findByPk(id);

    if (!order) {
        throw new Error("Registro não encontrado");
    };

    await order.destroy();
};