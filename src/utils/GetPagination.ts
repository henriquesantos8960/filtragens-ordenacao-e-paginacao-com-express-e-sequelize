import { OrderItem } from "sequelize/types";
import { Query } from "../shared/types/query";

export const getPagination = (id: number|string, query: Query) => {
    let {size, page, sort, order} = query; // dedestrurando a query para que possamos fazer a validação

    // Validação de parâmetros
    let limit = size ? size : undefined; // atribuindo valor à variável através de ternário
    let offset = page && size ? size * (page -1) : undefined;
    if (!sort) sort = id;
    if (!order) order = "asc";

    // Retorna objeto
    return {
        limit: limit,
        offset: offset,
        order: [[sort, order] as OrderItem],
    }
}