import {ProductInput, ProductOutput} from "../database/models/ProductsModel";
import * as repository from "../database/repositories/ProductsRepository";
import { Query } from "../shared/types/query";

export const getAll = async (
    lowerQuantityinStock: number, 
    higherQuantityinStock: number, 
    query: Query
): Promise<{rows: ProductOutput[], count: number}> => {
    return await repository.getAll(lowerQuantityinStock, higherQuantityinStock, query);
};

export const getById = async (id: string): Promise<ProductOutput> => {
    return await repository.getById(id);
};

export const create = async (payload: ProductInput): Promise<ProductOutput> => {
    return await repository.create(payload);
};

export const updateById = async (id: string, payload: ProductInput): Promise<ProductOutput> => {
    return await repository.updateById(id, payload);
};

export const deleteById = async (id: string): Promise<void> => {
    await repository.deleteById(id);
};