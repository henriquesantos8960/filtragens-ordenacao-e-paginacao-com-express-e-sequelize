import { OrderInput, OrderOutput } from "../database/models/OrdersModel";
import * as repository from "../database/repositories/OrdersRepository";
import { Query } from "../shared/types/query";


export const getAll = async (
    lowerOrderDate: string, 
    higherOrderDate: string, 
    query: Query
): Promise<{rows: OrderOutput[], count: number}> => {
    return await repository.getAll(lowerOrderDate, higherOrderDate, query);
};

export const getById = async (id: number): Promise<OrderOutput> => {
    return await repository.getById(id);
};

export const create = async (payload: OrderInput): Promise<OrderOutput> => {
    return await repository.create(payload);
};

export const updateById = async (id: number, payload: OrderInput): Promise<OrderOutput> => {
    return await repository.updateById(id, payload);
};

export const deleteById = async (id: number): Promise<void> => {
    await repository.deleteById(id);
};