import {Router} from "express";
import customers from "./CustomersRoute";
import employees from "./EmployeesRoute";
import offices from "./officesRoute";
import orders from "./ordersRoute";
import payments from "./paymentsRoute";
import productLines from "./productLinesRoute";
import products from "./productsRoute";

const routes = Router();

routes.use("/customers", customers);
routes.use("/employees", employees);
routes.use("/offices", offices);
routes.use("/orders", orders);
routes.use("/payments", payments);
routes.use("/productlines", productLines);
routes.use("/products", products);

export default routes;