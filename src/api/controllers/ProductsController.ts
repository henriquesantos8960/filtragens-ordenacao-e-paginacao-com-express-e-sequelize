import { Request, Response, NextFunction } from "express";
import * as service from "../../services/ProductsService"; 

export const getAll = async (req: Request, res: Response) => {
    const {size, page, sort, order, lowerQuantityInStock, higherQuantityInStock, ...filters} = req.query; // desestruturando a query da requisição, filters adiciona parâmetros "dinamicamente"

    // Alocando parâmetros da query dentro de um objeto
    const query = {
        size: parseInt(size as string), // size convertido para string depois para int
        page: parseInt(page as string),
        sort: sort as string,
        order: order as string,
        ...filters
    };

    res.send(await service.getAll(
        parseInt(lowerQuantityInStock as string), 
        parseInt(higherQuantityInStock as string), 
        query));
};

export const getById = async (req: Request, res: Response, next: NextFunction) => {
    res.send(await service.getById(String(req.params.id)));
};

export const create = async (req: Request, res: Response) => {
    res.status(201).send(await service.create(req.body));
};

export const updateById = async (req: Request, res: Response) => {
    res.send(await service.updateById(String(req.params.id), req.body));
};

export const deleteById = async (req: Request, res: Response) => {
    await service.deleteById(String(req.params.id))
    res.status(204).send();
}; 